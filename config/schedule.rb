# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron
require "net/http"
require "uri"

every :hour do
	command "/usr/bin/curl https://toastmoet.playappli.com/subscribes/wrapper", :output, {:error => 'error.log', :standard => 'cron.log'}
end

# Learn more: http://github.com/javan/whenever
