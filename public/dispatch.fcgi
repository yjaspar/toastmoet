#!/home/playapp2/.rbenv/versions/1.9.3-p448/bin/ruby

ENV['RAILS_ENV'] ||= 'preprod'
ENV['GEM_PATH'] ||= '/home/playapp2/.rbenv/versions/1.9.3-p448/lib/ruby/gems/1.9.1'
ENV['GEM_HOME'] ||= '/home/playapp2/.rbenv/versions/1.9.3-p448/lib/ruby/gems/1.9.1'

require 'rubygems'
require 'fcgi'

require File.join(File.dirname(__FILE__), '../config/environment')

class Rack::PathInfoRewriter
  def initialize(app)
    @app = app
  end

  def call(env)
    env.delete('SCRIPT_NAME')
    parts = env['REQUEST_URI'].split('?')
    env['PATH_INFO'] = parts[0]
    env['QUERY_STRING'] = parts[1].to_s
    @app.call(env)
  end
end

Rack::Handler::FastCGI.run  Rack::PathInfoRewriter.new(Toastmoet::Application)
