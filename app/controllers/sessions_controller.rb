require "instagram"

class SessionsController < ApplicationController
  def connect
    redirect_to Instagram.authorize_url(:redirect_uri => CALLBACK_URL)
  end

  def callback
    response = Instagram.get_access_token(params[:code], :redirect_uri => CALLBACK_URL)
    redirect_to :controller => 'subscribes', :action => 'wrapper', :access_token => response.access_token
  end
end