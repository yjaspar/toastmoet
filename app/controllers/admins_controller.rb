class AdminsController < ApplicationController
	before_filter :authorize

	private
 
	def authorize
		unless session[:user_id]
			redirect_to controller: 'subscribes', action: 'index'
		end
	end

	public

	def switch_website
		json = nil
		File.open("mod.json", "r") do |infile|
	        json = JSON.parse infile.gets
		    infile.close()
		end

		File.open("mod.json", "w+") do |infile|
			(json.has_key?('mod') && json['mod'] == 'game') ? infile.write('{"mod": "calendar"}') : infile.write('{"mod": "game"}')
			infile.close()
		end

		respond_to do |format|
			format.json { render json: {success: true} }
		end
	end

	def index
		render :index, :layout => 'admin_layout'
	end

	def subscribers
		cur = (params[:page] ? params[:page] : 1)
		@subscribers = Subscribe.fetch('date', cur, {:status => ['pending']})
		@subscribers[:current] = cur

		respond_to do |format|
            format.xls { send_data @subscribers[:data].to_xls, disposition: 'attachment', :filename => 'Subscribes.xls' }
            format.html {render :layout => 'admin_layout'}
        end
	end

	def moderate
		Subscribe.set({:id => params[:id]}, :status => params[:state])

		respond_to do |format|
			format.json { render json: {success: true} }
		end
	end

	def winners
		cur = (params[:page] ? params[:page] : 1)
		@subscribers = Subscribe.fetch('fame', cur, {:status => ['accepted']})
		@winners = Subscribe.fetch('fame', cur, {:status => ['won']})
		@subscribers[:current] = cur

		respond_to do |format|
            format.xls { send_data @winners[:data].concat(["", "", ""].concat(@subscribers[:data])).to_xls, disposition: 'attachment', :filename => 'moderated_and_winners.xls' }
            format.html {render :layout => 'admin_layout'}
        end
	end

	def grant
		Subscribe.set({:id => params[:id]}, {:status => params[:state]})
		Subscribe.set({:id => params[:id]}, {:updated_at => Time.now})
		respond_to do |format|
			format.json { render json: {success: true} }
		end
	end

	def notifications
		@subscribers = Subscribe.where('data.type' => 'facebook')
		render :notifications, :layout => 'admin_layout'
	end

	def users
		@admins = Admin.all
	    @_admin = Admin.new

	    render :users, :layout => 'admin_layout'
	end

	# POST /admins
	# POST /admins.json
	def create
	    @admin = Admin.new({:firstname => params['firstname'], :lastname => params['lastname'], :fb_userid => params['fb_userid']})
	    @admin.save
	    respond_to do |format|
	        format.html { redirect_to action: 'users', notice: 'Admin was successfully created.' }
	        format.json { render json: @admin, status: :created, location: @admin }
	    end
	end

	def destroy
	    @admin = Admin.find(params[:id])
	    @admin.destroy

	    respond_to do |format|
	        format.html { redirect_to action: 'users'}
	        format.json { head :no_content }
	    end
	end

	##
	## STATS
	##

	def get_percent_per_device
		@data = [['Device', 'Percentage']]

		@data.push(['Desktop', Subscribe.where('data.device' => 'desktop').count()])
		@data.push(['Mobile', Subscribe.where('data.device' => 'mobile').count()])
		@data.push(['Tablet', Subscribe.where('data.device' => 'tablet').count()])

		respond_to do |format|
            format.json { render json: @data }
        end
	end

	def get_votes_voters_per_month
		from = Time.strptime(params[:date_from],"%Y-%m-%d")
        to = Time.strptime(params[:date_to],"%Y-%m-%d")
        
        @data = [['Month', 'Votes', 'Voters']]

        @voters = []
        while from <= to do 
        	rec = Subscribe.where(:date => {'$gte' => from.utc, '$lt' => (from+1.days).utc})
        	rec.each do |r|
        		@voters.concat(r.voters) if (r.voters.size > 0)
        	end
        	@data.push([from.strftime('%d/%m/%Y'), 0, []])
        	from += 1.days
        end

        @data.each_with_index do |d, i|
        	if i > 0
        		tmp = Time.strptime(@data[i][0], "%d/%m/%Y")
        		@voters.each do |v|
        			if (v['date'] >= tmp && v['date'] <= (tmp + 1.days))
        				@data[i][1] += 1
        				@data[i][2] = v if !@data[i][2].include?(v)
        			end
        		end
        	end
        end

        @data.each_with_index do |d, i|
        	@data[i][2] = @data[i][2].size if (i > 0)
        end

        respond_to do |format|
            format.json { render json: @data }
        end
	end

	def get_participants_per_month
        from = Time.strptime(params[:date_from],"%Y-%m-%d")
        to = Time.strptime(params[:date_to],"%Y-%m-%d")
        @data = [['Month', 'Users', 'Facebook', 'Instagram', 'Validated']]
        
        i = 1
        while from <= to do
        	part = Subscribe.where(:date => {'$gte' => from.utc, '$lt' => (from+1.days).utc}).count()
        	fcb = Subscribe.where(:date => {'$gte' => from.utc, '$lt' => (from+1.days).utc}, 'data.type' => 'facebook').count()
        	inst = Subscribe.where(:date => {'$gte' => from.utc, '$lt' => (from+1.days).utc}, 'data.type' => 'instagram').count()
        	valid = Subscribe.where(:date => {'$gte' => from.utc, '$lt' => (from+1.days).utc}, :status => 'accepted').count()
        	@data.push([from.strftime('%d/%m/%Y'), 0, fcb, inst, valid])
        	i += 1
        	from += 1.days
        end

        respond_to do |format|
            format.json { render json: @data }
        end
    end
end
