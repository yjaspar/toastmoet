class SubscribesController < ApplicationController
	skip_before_filter :verify_authenticity_token, :only => [:create]
        protect_from_forgery :except => [:create]

    before_filter :p3p_header

    def p3p_header
        response.header['P3P:CP']="IDC DSP COR ADM DEVi TAIi PSA PSD IVAi IVDi CONi HIS OUR IND CNT"
    end

	def index
        require 'facebook'

        @request = params[:signed_request] ? Facebook::Parsor.signed_request(params[:signed_request], Settings.facebook.app_secret) : session[:request]
        @admin = Admin.find_by_fb_userid((@request["user_id"] if @request && @request.has_key?("user_id")))
        @conf = {
            request: {
                user_id: (@request['user_id'] if @request && @request.has_key?("user_id")),
                page: (@request['page'] if @request && @request.has_key?("page"))
            }, config: {
                facebook: {
                    app_id: Settings.facebook.app_id,
                    app_namespace: Settings.facebook.app_namespace,
                    app_perms: Settings.facebook.app_perms
                }
            },
            device: request.env['mobvious.device_type'].to_s
        }
        File.open("mod.json", "r+") do |infile|
            @conf['mod'] = JSON.parse(infile.gets)['mod']
            infile.close()
        end
        if @admin
            session[:user_id] = @admin.id
            session[:request] = @request
        end
        
        respond_to do |format|
            format.html # index.html.erb
        end
    end

    def form
        @subscribe = Subscribe.new(:data => {:type => 'facebook', :value => params, :device => request.env['mobvious.device_type'].to_s}, :date => Time.now, :voters => [], :rank => 0, :status => "pending")
        response = {success: (@subscribe.save ? true : false)}
        respond_to do |format|
            format.json { render json: response }
        end
    end

    def get_calendar
        headers['Access-Control-Allow-Origin'] = '*'
        headers['Access-Control-Request-Method'] = 'GET, OPTIONS, HEAD'
        headers['Access-Control-Allow-Headers'] = 'x-requested-with,Content-Type, Authorization'

        @obj = Subscribe.fetch_calendar()
        respond_to do |format|
            format.json { render json: @obj }
        end
    end

    def picture
        require 'mongo'
        require 'bson'

        @db = Mongo::MongoClient.new(Settings.db[Rails.env].host, Settings.db[Rails.env].port).db(Settings.db[Rails.env].name)
        @db.authenticate(Settings.db[Rails.env].auth.login, Settings.db[Rails.env].auth.password) if Settings.db[Rails.env].auth
        @grid = Mongo::Grid.new(@db)
        @data = @grid.get(BSON::ObjectId(params[:id]))
        send_data(@data.read, {
            :filename => @data.filename,
            :disposition => 'inline',
            :type => @data.content_type,

        })   
    end

    def vote
    	@subscribe = Subscribe.upvote(params[:id], params[:fbid])
    	respond_to do |format|
    		format.json { render json: @subscribe }
    	end
    end

    def get
        headers['Access-Control-Allow-Origin'] = '*'
        headers['Access-Control-Request-Method'] = 'GET, OPTIONS, HEAD'
        headers['Access-Control-Allow-Headers'] = 'x-requested-with,Content-Type, Authorization'
    	@subscribe = Subscribe.get(params[:id], params[:fbid])
    	respond_to do |format|
    		format.json { render json: @subscribe }
    	end
    end

    def create
    	require 'mongo'
        require 'mime/types'

        @db = Mongo::MongoClient.new(Settings.db[Rails.env].host, Settings.db[Rails.env].port).db(Settings.db[Rails.env].name)
        @db.authenticate(Settings.db[Rails.env].auth.login, Settings.db[Rails.env].auth.password) if Settings.db[Rails.env].auth
        @grid = Mongo::Grid.new(@db)

        @image = MiniMagick::Image.read(params[:Filedata].tempfile)
        if (request.env['HTTP_USER_AGENT'].downcase.match(/iphone|ipad/i))
            if(@image["EXIF:Orientation"]=='6')
                @image.combine_options do |c|
                    c.rotate "+90>"
                end
            elsif(@image["EXIF:Orientation"]=='3')
                @image.combine_options do |c|
                    c.rotate "+180>"
                end
            end
        end

        id = @grid.put(@image.to_blob(), :filename => params[:Filedata].original_filename, :content_type => MIME::Types.type_for(params[:Filedata].original_filename).first.to_s)
        respond_to do |format|
            format.json { render json: {:file_id => id} }
        end
    end

    def fetch
        headers['Access-Control-Allow-Origin'] = '*'
        headers['Access-Control-Request-Method'] = 'GET, OPTIONS, HEAD'
        headers['Access-Control-Allow-Headers'] = 'x-requested-with,Content-Type, Authorization'

    	@obj = Subscribe.fetch(params[:order], params[:page])
    	respond_to do |format|
            format.json { render json: @obj }
        end
    end

    def looping(config)
        t_start = Time.strptime(Settings.app.start_date, '%Y-%m-%d') # Date de debut du contest
        t_end = Time.strptime(Settings.app.end_date, '%Y-%m-%d') # Date de fin du contest
        
        client = Instagram.client(:access_token => Settings.instagram.token)
        posts = client.tag_recent_media(Settings.instagram.hashtag, config) # Recuperation des media recent sur le tag au choix
        skip = 0
        comment_tag = false
        posts.each do |post|
            if post.caption
                # Si l'entree existe deja ou si la future entree est anterieur a la date de debut du contest, ca veut dire qu'on peut s'arreter la
                # Si la publie est anterieur ca veut dire qu'on commence a fetcher des donnees useless
                # Si la publi existe deja, ca veut dire qu'on a deja fait le tour des futurs images car elle viennent du plus jeune au plus vieux
                logger.info(Time.at(post.caption.created_time.to_i))
                if (!Subscribe.find_by_instagram_post_id(post.id) && post.type == 'image' && Time.at(post.caption.created_time.to_i) > t_start && Time.at(post.caption.created_time.to_i) < t_end)
                    Subscribe.create(:data => {:type => 'instagram', :value => post, :device => 'mobile'}, :date => Time.at(post.caption.created_time.to_i), :voters => [], :rank => 0, :status => 'pending') # PUSH
                else
                    post.comments.data.each do |comment|
                        if(!Subscribe.find_by_instagram_post_id(post.id) && comment.text.downcase.include?("#toastmoet") && Time.at(comment.created_time.to_i) > t_start && Time.at(comment.created_time.to_i) < t_end )
                            Subscribe.create(:data => {:type => 'instagram', :value => post, :device => 'mobile'}, :date => Time.at(post.caption.created_time.to_i), :voters => [], :rank => 0, :status => 'pending') # PUSH
                            comment_tag = true
                        end
                    end
                    if(comment_tag == false) 
                    skip +=1
                    return false if (skip == 3)
                    end
                end
            end
        end

        # Render
        if (posts.pagination.next_max_id)
            looping({:max_id => posts.pagination.next_max_id})
        end
    end

	# Wraper - Recupere tous les posts sur instagram, methode utilise par le CRON
	def wrapper
        looping({})
        render :nothing => true
	end
end
