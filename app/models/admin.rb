class Admin
  	include MongoMapper::Document

	key :fb_userid, String
	key :firstname, String
	key :lastname, String
	timestamps!
	
end
