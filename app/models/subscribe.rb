class Subscribe
	include MongoMapper::Document

	key :data, Hash
	key :date, Time
	key :voters, Array
	key :rank, Integer
	key :updated_at, Time
	timestamps!

  	def self.find_by_instagram_post_id (post_id)
  		return self.where('data.value.id' => post_id).count>0?true:false
	end

	def self.get(id, fbid)
		raw = Subscribe.find(id)
		
		if raw.data['type'] == 'facebook'
			return {
				:id => raw.id,
				:full_name => raw.data['value']['first_name']+' '+raw.data['value']['last_name'],
				:username => raw.data['value']['fb_username'],
				:profile_picture => 'https://graph.facebook.com/'+raw.data['value']['fb_username']+'/picture',
				:picture => Settings.root_path+'subscribes/picture/'+raw.data['value']['picture_id'],
				:rank => raw.rank,
				:voted => (raw.voters.select {|x| x['fbid'] == fbid}.size > 0 ? true : false)
			}
		else
			return {
				:id => raw.id,
				:full_name => raw.data['value']['user']['full_name'],
				:username => raw.data['value']['user']['username'],
				:profile_picture => raw.data['value']['user']['profile_picture'],
				:picture => raw.data['value']['images']['low_resolution']['url'],
				:rank => raw.rank,
				:voted => (raw.voters.select {|x| x['fbid'] == fbid}.size > 0 ? true : false)
			}
		end
	end

	# In order to select a country, you have add a filter on `country` for facebook and add an $elemMatch request on data.users_in_photo for instagram
	def self.fetch(order, page, status={ :status => ['accepted', 'won'] })
		include ActionView::Helpers::TextHelper

		obj = {:size => Subscribe.all(status).count().to_i}
    	tmp = Subscribe.where(status).paginate({
    		:order => (order == 'date')?(:date.desc):(:rank.desc),
    		:per_page => 6,
    		:page => page
    	})
    	s = []

    	tmp.each_with_index do |p, i|
    		if p.data['type'] == 'facebook'
	    		s[i] = {
	    			:picture => Settings.root_path+'subscribes/picture/'+p.data['value']['picture_id'],
	    			:full_name => p.data['value']['first_name']+' '+p.data['value']['last_name'].truncate(17, :omission => "..."),
					:username => p.data['value']['fb_username'],
					:country => p.data['value']['country'],
					:profile_picture => 'https://graph.facebook.com/'+p.data['value']['fb_username']+'/picture'
	    		}
	    	else
	    		s[i] = {
	    			:picture => p.data['value']['images']['thumbnail']['url'],
	    			:full_name => p.data['value']['user']['full_name'].truncate(17, :omission => "..."),
	    			:profile_picture => p.data['value']['user']['profile_picture'],
					:username => p.data['value']['user']['username']
	    		}
	    	end

	    	s[i][:type] = p.data['type']
	    	s[i][:id] = p.id
	    	s[i][:date] = p.date.strftime('%d/%m/%Y @ %T')
	    	s[i][:status] = p.status
	    	s[i][:rank] = p.rank
	    	# s[i][:voted] = (p.voters.select {|x| x['fbid'] == fbid}.size > 0 ? true : false)

    	end
    	obj[:data] = s
    	return obj
	end

	def self.fetch_calendar()
		include ActionView::Helpers::TextHelper
		time = Time.new
		from = Time.strptime('2013-12-01',"%Y-%m-%d")
        to = Time.strptime('2013-12-03',"%Y-%m-%d") # Time.now()
    	rec = Subscribe.where(:date => {'$gte' => from.utc, '$lt' => (from+1.days).utc})
    	#tmp = where({'calendar' => {'$exists' => true}, 'calendar' => {'$gte' => from.utc, '$lt' => to.utc}}).sort({'calendar' => 1}).to_a
    	tmp = Subscribe.where({status:"won"}).limit(time.day).sort(:updated_at).to_a
    	#tmp = Subscribe.where({status:"won"}).limit(10).to_a
    	s = []

    	#tmp = []
    	#tmp[0] =  Subscribe.where({ data[value].user.username : "gracie916girl" }).limit(1).to_a
    	#tmp[1] =  Subscribe.where({ data[value].picture_id : "528262289161bdb118000002" }).limit(1).to_a


    	(0..30).to_a.each do |i|
    		logger.info(i)
			if i < tmp.size()
				if tmp[i].data['type'] == 'facebook'
		    		if  tmp[i].data['value']['fbid'] == '100004874378547'
						s[i] = {
			    			:picture => Settings.root_path+'subscribes/picture/'+tmp[i].data['value']['picture_id'],
			    			:full_name => "Moet & Chandon",
							:username => "moet",
							:profile_picture => 'https://scontent-b.xx.fbcdn.net/hphotos-frc3/525831_10150731658924793_1290054541_n.jpg'
			    		} 
					else
						s[i] = {
			    			:picture => Settings.root_path+'subscribes/picture/'+tmp[i].data['value']['picture_id'],
			    			:full_name => tmp[i].data['value']['first_name']+' '+tmp[i].data['value']['last_name'].truncate(17, :omission => "..."),
							:username => tmp[i].data['value']['fb_username'],
							:profile_picture => 'https://graph.facebook.com/'+tmp[i].data['value']['fb_username']+'/picture'
			    		} 
					end
		    	else
		    		s[i] = {
		    			:picture => tmp[i].data['value']['images']['thumbnail']['url'],
		    			:full_name => tmp[i].data['value']['user']['full_name'].truncate(17, :omission => "..."),
		    			:profile_picture => tmp[i].data['value']['user']['profile_picture'],
						:username => tmp[i].data['value']['user']['username']
		    		}
		    	end
		    else
		    	s[i] = {}
		    end
	    	s[i][:calendar] = ('December '+(i+1).ordinalize).gsub("th", "<sup>th</sup>").gsub("nd", "<sup>nd</sup>").gsub("rd", "<sup>rd</sup>").gsub("st", "<sup>st</sup>")
	    	s[i][:type] = tmp[i].data['type'] if tmp[i]
	    	s[i][:id] = tmp[i].id if tmp[i]
	    	s[i][:date] = tmp[i].date.strftime('%d/%m/%Y @ %T') if tmp[i]
	    	s[i][:status] = tmp[i].status if tmp[i]
	    	s[i][:rank] = tmp[i].rank if tmp[i]
	    	#s[i][:picture] = "https://toastmoet.playappli.com/subscribes/picture/529440a49161bd80e7000188"
	    	#s[i][:id] = "529440ab9161bd91e4000001"
    	end
    		
    	return s
	end

	def self.upvote(id, fbid)
		s_tmp = self.find(id)
		voted = false
		s_tmp.voters.each do |v|
			voted = true if v['fbid'] == fbid
		end
		if (!voted)
			s_tmp.rank+=1
			s_tmp.voters.push({:fbid => fbid, :date => Time.now})
			s_tmp.save
		end
		s_tmp['voted'] = true
		return s_tmp
	end
end
