// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require jquery.uploadifive.min
//= require jquery.uploadify.min
//= require underscore
//= require backbone
//= require backbone_rails_sync
//= require backbone_datalink
//= require backbone/toastmoet
//= require jquery.fancybox
//= require jquery.simplePagination
//= require jquery.cycle
//= require_tree ./backbone/


function extractUrlParams () {
	var t = location.search.substring(1).split('&');
	var f = [];
	for (var i=0; i<t.length; i++) {
		var x = t[ i ].split('=');
		f[x[0]]=x[1];
	}
	return f;
}

function mergeArray(root, add){
	for(x in root){
		console.log(x);
		if (add[x])
			root[x] = add[x];
	}
	return (root);
}

/*********
 * POPUP *
 *********/
function popupPerso(param)
{
	param = param || {};
	var settings = 
	{
		fkColor     : 'rgba(0,0,0, 0.7)',
		zindex      : 7000,
		content     : 'ma popup !',
		width       : '300px'

	};
	$.extend(settings, param);
	var template = '<div id="popupPerso" style="width:'+settings.width+';z-index:'+(settings.zindex+1)+';">';
	template    += '<a href="javascript:void(0);" id="close">X</a><div id="blockContentPopupPerso">'; 
	template    += '</div>';
	template    += '</div>';
	
	var timer = null;
	var obj   = this;
	
	var addFK = function()
	{
		$('body').append('<div id="fkPopupPerso" style="display:none;position:fixed;z-index:'+settings.zindex+';top:0;left:0;width:100%;height:100%;background-color:'+settings.fkColor+';"></div>');	

	}
	
	var initContent = function()
	{
		$('#fkPopupPerso').html(template);
		$('#blockContentPopupPerso').html('');
		$('#blockContentPopupPerso').append(settings.content);
	}
	
	this.setTopPosition = function()
	{
		FB.Canvas.getPageInfo(function(info) 
		{
			$('#popupPerso').css('margin-top', (info.scrollTop + 10)+'px');	
		});
	}
	
	this.setOnAutoPos = function()
	{
		timer = setInterval(obj.setTopPosition,100);		
	}
	
	this.show = function()
	{
		addFK();
		initContent();
		obj.setOnAutoPos();
		$('#croix_popupPerso').click($.proxy(this.destroy, this));
		$('#buttonsPopupUpload #annuler').unbind();
		$('#buttonsPopupUpload #annuler').click($.proxy(this.destroy, this));
		$('#fkPopupPerso').fadeIn('fast');
		$("#close").click(function(){obj.destroy();});	
	}
	
	this.destroy = function(callback)
	{	
        clearInterval(timer);    
		$('#fkPopupPerso').fadeOut('fast', function()
		{
			$('#fkPopupPerso').remove();
			if(callback)callback();
		});
	}
}
