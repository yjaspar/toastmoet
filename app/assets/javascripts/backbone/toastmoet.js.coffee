#= require_self
#= require_tree ./templates
#= require_tree ./models
#= require_tree ./views
#= require_tree ./routers

window.Toastmoet =
  Models: {}
  Collections: {}
  Routers: {}
  Views: {}

$ ->
  window.location.href = window.location.href.replace /#(.*)_=_/, '&loose=_' if window.location.href.match /#(.*)_=_/