class Toastmoet.Models.Subscribe extends Backbone.Model
  	paramRoot: 'subscribe'

	defaults:
	    firstname: null
	    lastname: null
	    email: null
	    birthdate: null
	    use_conditions: null
	    fb_userid: null

class Toastmoet.Collections.SubscribesCollection extends Backbone.Collection
	model: Toastmoet.Models.Subscribe
	url: '/subscribes'