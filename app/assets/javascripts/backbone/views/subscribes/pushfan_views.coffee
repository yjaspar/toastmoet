Toastmoet.Views.Subscribes ||= {}

class Toastmoet.Views.Subscribes.PushFanView extends Backbone.View
    template: JST["backbone/templates/subscribes/push_fan"]
    events: {'click #btn_enter': 'getLogin'}

    initialize: (data) ->
        @data = data.object

    getLogin: () ->
        _gaq.push ['_trackEvent', 'Facebook_Tab', 'Participate', null, null, true]
        top.location.href = "https://apps.facebook.com/"+@data.config.facebook.app_namespace

    render: ->
        @$el.html @template(data: @data) 
        return @
