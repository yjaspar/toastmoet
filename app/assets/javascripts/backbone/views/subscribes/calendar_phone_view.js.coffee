Toastmoet.Views.Subscribes ||= {}

class Toastmoet.Views.Subscribes.CalendarPhoneView extends Backbone.View
    template: JST["backbone/templates/subscribes/calendar_phone"]
    events: {
        "click .zoom-picture": "printPicture",
    }

    initialize: (params) =>
        @params = params
        @loadGallery 1

    getDim: (img) ->
        h = img.height
        w = img.width
        if (h > w)
            if h > 480
                w = w / (h / 480)
            h = if h > 480 then 480 else h
            h += 100
        else
            if w > 480
                h = h / (w / 480) + 100
            if w < 200
                h = h / (w / 200) + 100
            w = if w > 480 then 480 else (if w < 200 then 200 else w)

        return {height: h, width: w}

    printPicture: (e) ->
        self = @
        id = (if typeof e == "string" then e else e.target.parentNode.parentNode.getAttribute('id'))
        opt = {'url': '/subscribes/'+id+'.json?fbid=1', 'dataType': 'json'}
        opt.success = (object) ->
            object.meta = self.params.config
            load = new Image();
            load.src = object.picture;
            load.onload = () ->
                dim = self.getDim load
                #$.fancybox.open {
                #    autoSize: false,
                #    autoDimensions: false,
                #    width: dim.width,
                #    height: dim.height,
                #    type: 'ajax',
                #    afterLoad: () ->
                #    #    start();
                #    #    $('.fancybox-wrap').css('display', 'none');
                #    #    $('canvas').css('margin-top', 0);
                #    #    $('div.fancybox-overlay').css('background', 'none');
                #        setTimeout () ->
                #            FB.Canvas.getPageInfo (info) ->
                #                h = parseInt($('.fancybox-wrap').css('height').replace(/px/, ''));
                #                $('.fancybox-wrap').css('top', ((info.clientHeight - h)/2)+40+info.scrollTop).fadeIn('fast');
                #                console.log dim.height
                #                $('fancybox-inner').css('height', dim.height);
                #        , 500
                #    ,
                #    #beforeClose: () ->
                #    #    $('canvas').fadeOut('fast');
                #    #,
                #    content: JST['backbone/templates/subscribes/picture_phone'] object
                #}
                #$.fancybox.open {
                #    autoDimensions: true,
                #    type: 'ajax',
                #    content: JST['backbone/templates/subscribes/picture_phone'] object
                #}
                popUp = new popupPerso({
                    width: '225px', 
                    content: JST['backbone/templates/subscribes/picture_phone'] object
                })
                popUp.show()
        $.ajax opt

    loadGallery: (current) ->
        url = '/subscribes/calendar.json'
        opt = {'url': url, 'dataType': 'json'}

        self = @
        opt.success = (object) ->
            self.displayPictures object
        opt.error = (data) ->
            alert 'Une erreure est survenue lors du chargement de la galerie, merci de contacter un administrateur'
        $.ajax opt

    displayPictures: (p) ->
        html = ''
        i = 1
        console.log((p.length-1));

        for doc in p
            do (doc) ->
                if doc.id
                    $('.pola-big').find('img').attr('src', doc.picture);
                    $('.pola-big').find('p').html(doc.calendar);


                html += "<div class='_image_display_' style='margin: 5px 5px 30px;'>"
                html += "   <div class='fontAvenirLTStdLight fgwhite' style='text-align: left;'>"
                html += "   <div class='discovered-bar'></div>"
                html += "   <p class='date-zone'>"+doc.calendar+"<p>"
                html += "   </div>"
                html += "   <div>"
                html += "       <div class='thumbnail_' style>"
                if doc.id
                    html += "       <div class='overlay-picture fggold fontCopper1' id='"+doc.id+"'>"
                    html += "           <div style='margin-top: 40px;'>ZOOM<br />"
                    html += "           <img src='/assets/zoom.png' class='zoom-picture' /></div>"
                    html += "       </div>"
                if doc.picture
                     html += "       <div class='overflow' style='background-image:url("+doc.picture.replace("http://", "https://")+"); background-size:cover; background-position:center;'>"
                    #html += "               <img class='pic' src='"+doc.picture+"' />"
                else
                    html += "           <div class='overflow'>"
                    html += "               <div style='position: relative; width: 200px; height: 200px; background-color: #191919; margin: auto;border-radius:6px;'>"
                    html += "                   <div style='position: absolute; margin-left: 15px; font-size: 85px; font-family: Arial; color: #fff; margin-top: 0;'>"+i+"</div>"
                    html += "                   <img class='bottle' src='/assets/bottle.png' style='position:absolute;bottom:50px;right:30px;' />"
                    html += "                   <img class='corner' src='/assets/flipped-corner.jpg' style='position:absolute;bottom:0;right:0;' />"
                    html += "               </div>"
                html += "           </div>"
                html += "       </div>"
                html += "   </div>"
                html += "</div>"
                i += 1
        $('#center-slider').html html
            


    render: ->
        @$el.html @template
        return @
