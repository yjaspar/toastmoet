Toastmoet.Views.Subscribes ||= {}

class Toastmoet.Views.Subscribes.IndexView extends Backbone.View
    template: JST["backbone/templates/subscribes/index"]
    events: {'click #btn_enter': 'getLogin'}

    initialize: =>
        # Initialization

    getLogin: () ->
        _opt = @options
        FB.login((response) ->
            if response.authResponse
                Backbone.history.navigate "#form", {trigger: true}
            else
                Backbone.history.navigate '#error', {trigger: true}
            return
        , {scope : @options.config.facebook.app_perms })

    render: ->
        @$el.html @template(data: @options) 
        return @
