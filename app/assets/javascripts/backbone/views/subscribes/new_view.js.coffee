Toastmoet.Views.Subscribes ||= {}

class Toastmoet.Views.Subscribes.NewView extends Backbone.View
    template: JST["backbone/templates/subscribes/new"]
    events: {'submit #form_participate': 'save'}

    initialize: (options) ->
        @model = new @collection.model firstname: options.data.first_name, lastname: options.data.last_name, email: options.data.email, fb_userid: options.data.id, birthdate: null, use_conditions: null
        @options = options

    save: (e) ->
        self = @
        $('#app div').fadeOut 'fast'
        setTimeout () ->
            $('#app center').fadeIn 'fast', () ->
                self.collection.create self.model.toJSON(),
                    success: (bookmark) =>
                        Backbone.history.navigate "#social", {trigger: true}
                    error: (bookmark, jqXHR) =>
                    $('#app center').fadeOut 'fast', () ->
                        $('#app div').fadeIn 'fast'
        , 200

    render: ->
        @$el.html @template data: @model.toJSON()
        @.$("form").backboneLink @model
        return @
