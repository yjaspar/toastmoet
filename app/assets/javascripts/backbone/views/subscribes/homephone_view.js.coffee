Toastmoet.Views.Subscribes ||= {}

class Toastmoet.Views.Subscribes.HomePhoneView extends Backbone.View
    template: JST["backbone/templates/subscribes/home_phone"]
    events: {
        "click #new_gallery": "new",
        "click #famous_gallery": "famous",
        "click .zoom-picture": "printPicture",
        "click #gallery_link": "showGallery",
        "click #win_link": "showWin",
        "click #participate_link": "showParticipate"
    }

    initialize: (params) =>
        @params = params
        @order = 'date'
        @max = 0
        @page = 1
        @scroll = true
        @loadGallery()
                

    famous: () ->
        $('#new_gallery').removeClass('active')
        $('#famous_gallery').addClass('active')
        @order = 'fame'
        @page = 1
        @loadGallery()

    new: () ->
        $('#famous_gallery').removeClass('active')
        $('#new_gallery').addClass('active')
        @order = 'date'
        @page = 1
        @loadGallery()

    showGallery: () ->
        @scroll = true
        _gaq.push ['_trackEvent', 'Facebook_App', 'Gallery', null, null, true]
        $('#menu-tab ul li.active').removeClass('active');
        $('#gallery_link').addClass('active');
        $('.row_home').fadeOut 'fast'
        setTimeout () ->
            $('#gallery').fadeIn 'fast'
        , 200

    showWin: () ->
        @scroll = false
        _gaq.push ['_trackEvent', 'Facebook_App', 'Prizes', null, null, true]
        $('#menu-tab ul li.active').removeClass('active');
        $('#win_link').addClass('active');
        $('.row_home').fadeOut 'fast'
        setTimeout () ->
            $('#prizes').fadeIn 'fast'
        , 200

    showParticipate: () ->
        @scroll = false
        _gaq.push ['_trackEvent', 'Facebook_App', 'Participate', null, null, true]
        $('#menu-tab ul li.active').removeClass('active');
        $('#participate_link').addClass('active');
        $('.row_home').fadeOut 'fast'
        setTimeout () ->
            $('#participate').fadeIn 'fast'
        , 200

    votePicture: (e) ->
        self = @
        FB.getLoginStatus (response) ->
            if response.authResponse
                FB.api '/me', (res) ->
                    id = e.target.parentNode.parentNode.getAttribute('id')
                    opt = {url: "/subscribes/"+id+"/vote.json", data: {'fbid': res.id}, dataType: 'json', type: 'post'}
                    opt.success = () ->
                        e.target.src = '/assets/vote-done.png'
                    $.ajax opt
            else
                FB.login((response) ->
                    if response.authResponse
                        self.votePicture(e)
                    else
                        $.fancybox.open {
                            autoDimensions: true,
                            width: '100%'
                            type: 'ajax',
                            beforeShow: () ->
                                this.skin.css("background-color", "black")
                            content: JST['backbone/templates/subscribes/perms']
                        }
                    return
                , {scope : self.params.config.config.facebook.app_perms })

    printPicture: (e) ->
        self = @
        #id = (if typeof e == "string" then e else e.target.parentNode.getAttribute('id'))
        id =  (if typeof e == "string" then e else e.currentTarget.getAttribute('id'))
        opt = {'url': '/subscribes/'+id+'.json', 'dataType': 'json'}
        opt.success = (object) ->
            object.meta = self.params.config
            #$.fancybox.open {
            #    autoDimensions: true,
            #    type: 'ajax',
            #    content: JST['backbone/templates/subscribes/picture_phone'] object
            #}
            popUp = new popupPerso({
                width: '225px', 
                content: JST['backbone/templates/subscribes/picture_phone'] object
            })
            popUp.show()
        $.ajax opt
        
    loadGallery: () ->
        console.log @page
        url = '/subscribes/'+@order+'/'+@page+'.json'
        opt = {'url': url, 'dataType': 'json'}

        self = @
        opt.success = (object) ->
            self.displayPictures object.data
            self.max = object.size
            if self.params.config.uri['id_photo']
                self.printPicture(self.params.config.uri['id_photo'])
                self.params.config.uri['id_photo'] = null
        opt.error = (data) ->
            alert 'Une erreure est survenue lors du chargement de la gallerie, merci de contacter un administrateur'
        $.ajax opt

    displayPictures: (p) ->
        self = @
        html = ''
        i = 0;
        for doc in p
            do (doc) ->
                html += '<div class="picture">';
                html += '    <div class="thumbcover zoom-picture" id="'+doc.id+'" style="background-image:url('+doc.picture.replace("http://", "https://")+'); background-size:cover;  background-position:center;">';    
                #html += '       <img src="'+doc.picture.replace("http://", "https://")+'" alt="" />';
                html += '    </div>';
                html += '    <div class="info_user">';
                html += '        <img src="'+doc.profile_picture+'" alt="" class="left"/>';
                html += '        <p class="left">';
                html += '            <span class="fgwhite">Picture added by</span><br/>';
                html += '            <span class="fggold">'+doc.full_name+'</span>';
                html += '        </p>';
                html += '        <div class="clear"></div>';
                html += '    </div>';
                html += '</div>';
                i++;
                if (i%2 == 0)
                    html += '<div class="clear"></div>';
        
        if (@page == 1) then $('#pictures').html html else $('#pictures').append html

        load = false
        $(window).scroll () ->
            if($('.picture:last').offset()? && $('.picture')?)
                if (self.scroll && $(window).scrollTop() + $(window).height()) >= $('.picture:last').offset().top && !load && $('.picture').length < self.max
                    self.page += 1
                    load = true
                    self.loadGallery()
            


    render: ->
        @$el.html @template
        return @
