Toastmoet.Views.Subscribes ||= {}

class Toastmoet.Views.Subscribes.HomeView extends Backbone.View
    template: JST["backbone/templates/subscribes/home"]
    events: {
        "click #previous_gallery": "previous",
        "click #next_gallery": "next",
        "click #new_gallery": "new",
        "click #famous_gallery": "famous",
        "click #participate-button": "askPerms",
        "click .zoom-picture": "printPicture",
        "click #gallery_link": "showGallery",
        "click #win_link": "showWin",
        "click #participate_link": "showParticipate"
    }

    initialize: (params) =>
        @params = params
        @order = 'date'
        @loadGallery 1

    famous: () ->
        $('#new_gallery').removeClass('fgwhite').addClass('fggold')
        $('#new_gallery img').attr('src', '/assets/arrow-right-gold.png')
        $('#famous_gallery').removeClass('fggold').addClass('fgwhite')
        $('#famous_gallery img').attr('src', '/assets/arrow-right-white.png')
        @order = 'fame'
        @loadGallery 1

    new: () ->
        $('#famous_gallery').removeClass('fgwhite').addClass('fggold')
        $('#famous_gallery img').attr('src', '/assets/arrow-right-gold.png')
        $('#new_gallery').removeClass('fggold').addClass('fgwhite')
        $('#new_gallery img').attr('src', '/assets/arrow-right-white.png')
        @order = 'date'
        @loadGallery 1

    previous: () ->
        $('#paginate').pagination('prevPage');

    next: () ->
        $('#paginate').pagination('nextPage');

    showGallery: () ->
        _gaq.push ['_trackEvent', 'Facebook_App', 'Gallery', null, null, true]
        $('.button-menu').removeClass('fgwhite').addClass('fgbrown')
        $('#gallery_link').removeClass('fgbrown').addClass('fgwhite')
        $('.row_home').fadeOut 'fast'
        setTimeout () ->
            $('#slider-section').fadeIn 'fast'
        , 200

    showWin: () ->
        _gaq.push ['_trackEvent', 'Facebook_App', 'Prizes', null, null, true]
        $('.button-menu').removeClass('fgwhite').addClass('fgbrown')
        $('#win_link').removeClass('fgbrown').addClass('fgwhite')
        $('.row_home').fadeOut 'fast'
        setTimeout () ->
            $('#win-section').fadeIn 'fast'
        , 200

    showParticipate: () ->
        _gaq.push ['_trackEvent', 'Facebook_App', 'Participate', null, null, true]
        $('.button-menu').removeClass('fgwhite').addClass('fgbrown')
        $('#participate_link').removeClass('fgbrown').addClass('fgwhite')
        $('.row_home').fadeOut 'fast'
        setTimeout () ->
            $('#participate-section').fadeIn 'fast'
        , 200

    askPerms: () ->
        self = @
        FB.getLoginStatus (response) ->
            if response.authResponse
                FB.api '/me', (res) ->
                    $.fancybox.open {
                        autoDimensions: false,
                        width : '320',
                        type: 'ajax',
                        hideOnOverlayClick: false,
                        beforeClose: () ->
                            if navigator.userAgent.match(/MSIE 9.0/) then window.top.location.href = 'https://apps.facebook.com/'+self.params.config.config.facebook.app_namespace
                        ,
                        beforeShow: () ->
                            this.skin.css("background-color", "black")
                        ,
                        content: JST['backbone/templates/subscribes/form'] res
                    }
            else
                FB.login((response) ->
                    if response.authResponse
                        self.askPerms()
                    return
                , {scope : self.params.config.config.facebook.app_perms })

    getDim: (img) ->
        h = img.height
        w = img.width
        if (h > w)
            h = if h > 480 then 480 else h
            w = 'auto'
            h += 90
        else
            w = if w > 480 then 480 else (if w < 200 then 200 else w)
            h = 'auto'

        return {height: h, width: w}

    printPicture: (e) ->
        self = @
        id = (if typeof e == "string" then e else e.target.parentNode.parentNode.getAttribute('id'))
        opt = {'url': '/subscribes/'+id+'.json?fbid=1', 'dataType': 'json'}
        opt.success = (object) ->
            object.meta = self.params.config
            load = new Image();
            load.src = object.picture;
            load.onload = () ->
                dim = self.getDim load
                #$.fancybox.open {
                #    autoSize: false,
                #    autoDimensions: false,
                #    width: dim.width,
                #    height: dim.height,
                #    type: 'ajax',
                #    content: JST['backbone/templates/subscribes/picture'] object
                #
                #}
                popUp = new popupPerso({
                    width: '500px', 
                    content: JST['backbone/templates/subscribes/picture'] object
                })
                popUp.show()


                
        $.ajax opt

    loadGallery: (current) ->
        url = '/subscribes/'+@order+'/'+current+'.json'
        opt = {'url': url, 'dataType': 'json'}

        self = @
        opt.success = (object) ->
            self.displayPictures object.data
            $('#paginate').pagination({
                items: parseInt(object.size),
                itemsOnPage: 6,
                currentPage: current,
                cssStyle: 'moet-theme'
                nextText: '',
                prevText: ''
                onPageClick: (page_n, e) ->
                    self.loadGallery page_n
            })
            if self.params.config.uri['id_photo']
                self.printPicture(self.params.config.uri['id_photo'])
                self.params.config.uri['id_photo'] = null
        opt.error = (data) ->
            alert 'Une erreur est survenue lors du chargement de la galerie, merci de contacter un administrateur'
        $.ajax opt

    displayPictures: (p) ->
        html = ''
        for doc in p
            do (doc) ->
                html += "<div class='_image_display_'>"
                html += "   <div class='overlay-picture fggold fontCopper1' id='"+doc.id+"'>"
                html += "   <div style='margin-top: 40px;'>ZOOM<br />"
                html += "   <img src='/assets/zoom.png' class='zoom-picture' /></div>"
                html += "   </div>"
                html += "   <div class='thumbnail_'>"
                html += "   <div class='overflow' style='background-image:url("+doc.picture.replace("http://", "https://")+"); background-size:cover; background-position:center;'>"
                # html += "   <img class='pic' src='"+doc.picture.replace("http://", "https://")+"' />"
                html += "   </div>"
                html += "   </div>"
                html += "   <div class='profile'>"
                html += "       <img src='"+doc.profile_picture+"' />"
                html += "       <div class='fgwhite fontAvenirLTStdLight'>"
                html += "           Picture added by<br />"
                html += "           <span class='fggold'>"+doc.full_name+"</span>"
                html += "           <div style='clear: both'></div>"
                html += "       </div>"
                html += "   <div style='clear: both'></div>"
                html += "   </div>"
                html += "</div>"

        $('#center-slider').html html
            


    render: ->
        @$el.html @template
        return @