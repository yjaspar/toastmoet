Toastmoet.Views.Subscribes ||= {}

class Toastmoet.Views.Subscribes.EditView extends Backbone.View
  template: JST["backbone/templates/subscribes/edit"]

  events:
    "submit #edit-subscribe": "update"

  update: (e) ->
    e.preventDefault()
    e.stopPropagation()

    @model.save(null,
      success: (subscribe) =>
        @model = subscribe
        window.location.hash = "/#{@model.id}"
    )

  render: ->
    @$el.html(@template(@model.toJSON() ))

    this.$("form").backboneLink(@model)

    return this
