Toastmoet.Views.Subscribes ||= {}

class Toastmoet.Views.Subscribes.ErrorView extends Backbone.View
    template: JST["backbone/templates/subscribes/error"]

    initialize: (@options) ->
        this.render

    events: {
        'click #btn_enter_popup': 'getLogin',
    }

    getLogin: () ->
        _opt = @options
        FB.login((response) ->
            if response.authResponse
                _opt.user = response.authResponse
            else
                Backbone.history.navigate '#error', {trigger: true}
            return
        , {scope : @options.config.facebook.app_perms })

    render: =>
        @$el.html(@template(data: @options, message: 'Pwet for the moment'))
        return this
