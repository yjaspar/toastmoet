Toastmoet.Views.Subscribes ||= {}

class Toastmoet.Views.Subscribes.SubscribeView extends Backbone.View
  template: JST["backbone/templates/subscribes/subscribe"]

  events:
    "click .destroy" : "destroy"

  tagName: "tr"

  destroy: () ->
    @model.destroy()
    this.remove()

    return false

  render: ->
    @$el.html(@template(@model.toJSON() ))
    return this
