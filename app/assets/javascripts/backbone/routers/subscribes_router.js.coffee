class Toastmoet.Routers.SubscribesRouter extends Backbone.Router
    initialize: (options) ->
        @subscribes or= new Toastmoet.Collections.SubscribesCollection()
        @_options = options
        # extractUrlParams -> javascripts/application.js
        @_options.uri = extractUrlParams()

    routes:
        "index"             : "index"
        "pushFan"           : "push_fan"
        "form"              : "dispForm"
        "calendar"          : "calendar"
        "home"              : "home"
        "home/:id"          : "home"
        "error"             : "error"
        ".*"                : "index"
        "*test"             : "index"
        "form_test"         : "form_test"

    index: ->
        if(location.search == "?test=form")
            @home()
            return
        if @_options.request.page
            window.top.location = 'https://apps.facebook.com/toast_moet/'
            return
            
        if @_options.uri.page == 'calendar' || @_options.mod == 'calendar'
            @calendar()
        else
            if !@_options.request.page || document.URL.search("index_mobile") != -1
                @home() 
            else 
                @push_fan()

    index_mobile: ->
        if @_options.uri.page == 'calendar' || @_options.mod == 'calendar'
            @calendar()
        else
            @home() 

    push_fan: ->
        self = @
        $("#app div").html ''
        $("#app center").fadeOut 'fast', () ->
            self.view = new Toastmoet.Views.Subscribes.PushFanView(object: self._options)
            $("#app div").html(self.view.render().el)

    dispForm: ->
        self = @
        console.log(self)
        $("#app div").html ''
        $("#app center").fadeIn 'fast', () ->
        FB.getLoginStatus (response) ->
            console.log(response)
            if response.authResponse
                FB.api 'me', (data) ->
                    self.view = new Toastmoet.Views.Subscribes.FormPhoneView(data: data, collection: self.subscribes)
                    $("#app center").fadeOut 'fast', () ->
                        $('#app div').html(self.view.render().el)
            else
                FB.login((response) ->
                    if response.authResponse
                        self.dispForm()
                    return
                , {scope : self.view.params.config.config.facebook.app_perms })

    dispIndex: ->
        self = @
        @view = new Toastmoet.Views.Subscribes.IndexView(@_options)
        $("#app center").fadeOut 'fast', () ->
            $('#app div').html(self.view.render().el)

    error: ->
        @view = new Toastmoet.Views.Subscribes.ErrorView(@_options, @subscribes)
        $("#app div").html(@view.render().el)

    calendar: ->
        self = @
        $("#app div").html ''
        $("#app center").fadeOut 'fast', () ->
            if self._options.device == 'mobile'
                self.view = new Toastmoet.Views.Subscribes.CalendarPhoneView(config: self._options)
            else
                self.view = new Toastmoet.Views.Subscribes.CalendarView(config: self._options)
            $("#app div").html(self.view.render().el)

    home: (id) ->
        self = @
        $("#app div").html ''
        $("#app center").fadeOut 'fast', () ->
            if self._options.device == 'mobile'
                self.view = new Toastmoet.Views.Subscribes.HomePhoneView(config: self._options, collection: self.subscribes)
            else
                self.view = new Toastmoet.Views.Subscribes.HomeView(config: self._options, collection: self.subscribes)
            $("#app div").html(self.view.render().el)
